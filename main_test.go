package main

import (
	"bytes"
	"fmt"
	"os"
	"reflect"
	"testing"

	"github.com/editorconfig/editorconfig-core-go/v2"
	"github.com/sergi/go-diff/diffmatchpatch"
	"golang.org/x/net/html"
)

/*
func TestStruct(t *testing.T) {
	die := func(err error) {
		if err != nil {
			t.Fatal(err)
		}
	}

	want, err := os.Open("testdata/nice.html")
	die(err)

	original, err := os.Open("testdata/original.html")
	die(err)

	got := bytes.Buffer{}
	err = Nicen(original, &got)
	die(err)

	// compare reader contents

	left, err := io.ReadAll(want)
	die(err)

	right, err := io.ReadAll(&got)
	die(err)

	if !reflect.DeepEqual(left, right) {
		t.Fatalf("want %s but got %s\n", left, right)
	}
}
*/

func TestParseRender(t *testing.T) {
	buf, err := os.ReadFile("testdata/original.html")
	if err != nil {
		t.Fatal(err)
	}

	doc, err := html.Parse(bytes.NewReader(buf))
	if err != nil {
		t.Fatal(err)
	}

	var f func(*html.Node, uint)
	f = func(e *html.Node, level uint) {
		for c := e.FirstChild; c != nil; c = c.NextSibling {
			f(c, level+1)
		}
	}
	f(doc, 0)

	/*
		if err := html.Render(os.Stdout, doc); err != nil {
			t.Fatal(err)
		}
	*/
}

func TestTidy(t *testing.T) {
	t.Skip("tidy.html is created manually, skip for now")

	// want
	buf1, err := os.ReadFile("testdata/tidy.html")
	if err != nil {
		t.Fatal(err)
	}
	want := string(buf1)

	// got
	r, err := os.Open("testdata/original.html")
	if err != nil {
		t.Fatal(err)
	}
	w := bytes.Buffer{}
	if err := Tidy(r, &w); err != nil {
		t.Fatal(err)
	}
	got := string(w.Bytes())

	if got != want {
		fmt.Printf("want:\n%s\n", want)
		fmt.Printf("got :\n%s\n", got)
		dmp := diffmatchpatch.New()
		diffs := dmp.DiffMain(got, want, false)
		s := dmp.DiffPrettyText(diffs)
		t.Fatalf("found a difference: %s", s)
	}
}

func TestMerge(t *testing.T) {
	const (
		filename         = "testdata/merge.html"
		originalChildren = 1 + 2*13 // newline, { <p/>, newline }
		tidyChildren     = 1 + 2*5  // newline, { <p/>, newline }
	)

	die := func(err error) {
		if err != nil {
			t.Fatal(err)
		}
	}

	doc, err := document(filename)
	die(err)

	if doc.Type != html.DocumentNode {
		t.Fatalf("want document node but got %+v", doc)
	}
	html_ := doc.FirstChild
	if html_.Data != "html" {
		t.Fatalf("want %q but got %q", "html", html_.Data)
	}

	head := html_.FirstChild // created by parser as default node
	if head.Data != "head" {
		t.Fatalf("want %q but got %q", "head", head.Data)
	}
	body := html_.LastChild
	if body.Data != "body" {
		t.Fatalf("want %q but got %q", "body", body.Data)
	}

	crlf := body.FirstChild
	if hasText(crlf) {
		t.Fatalf("want whitespace node but got %q", crlf.Data)
	}

	n := CountChildren(body)
	if n != originalChildren {
		t.Fatalf("want %d original children but got %d", originalChildren, n)
	}

	f, err := os.Open(filename)
	die(err)
	defer f.Close()

	buf := bytes.Buffer{}
	if err := Tidy(f, &buf); err != nil {
		t.Fatal(err)
	}

	doc, err = html.Parse(&buf)
	die(err)
	body = doc.FirstChild.LastChild
	n = CountChildren(body)
	if n != tidyChildren {
		t.Fatalf("want %d tidy children but got %d", tidyChildren, n)
	}

	want, err := os.ReadFile("testdata/merged.html")
	die(err)

	var gotBuf bytes.Buffer
	if err := html.Render(&gotBuf, doc); err != nil {
		die(err)
	}

	// final newline according to .editorconfig
	def, err := editorconfig.GetDefinitionForFilename(filename)
	if err != nil {
		t.Fatal(err)
	}
	finalNL := def.InsertFinalNewline
	if finalNL == nil {
		t.Fatalf("missing .editorconfig definition for InsertFinalNewline")
	}
	if *finalNL {
		gotBuf.Write([]byte{'\n'})
	}
	got := gotBuf.Bytes()

	if !reflect.DeepEqual(want, got) {
		t.Fatalf("want\n%q\nbut got\n%q", want, got)
	}
}

func CountChildren(e *html.Node) uint {
	if e.FirstChild == nil {
		return 0
	}

	var n uint
	for c := e.FirstChild; c != e.LastChild; c = c.NextSibling {
		n++
	}
	return n + 1
}

func document(filename string) (*html.Node, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return html.Parse(f)
}

func TestEditorconfig(t *testing.T) {
	const want = true

	def, err := editorconfig.GetDefinitionForFilename("testdata/original.html")
	if err != nil {
		t.Fatal(err)
	}

	got := def.InsertFinalNewline
	if got == nil {
		t.Fatalf("missing definition for InsertFinalNewline")
	}
	if want != *got {
		t.Fatalf("want %t but got %t", want, *got)
	}
}

func TestTidyOvertown(t *testing.T) {
	buf1, err := os.ReadFile("testdata/overtown-tidy.html")
	if err != nil {
		t.Fatal(err)
	}
	want := string(buf1)

	r, err := os.Open("testdata/overtown-original.html")
	if err != nil {
		t.Fatal(err)
	}
	w := bytes.Buffer{}
	if err := Tidy(r, &w); err != nil {
		t.Fatal(err)
	}
	got := string(w.Bytes())

	if got != want {
		fmt.Printf("got:\n--------\n%s\n--------\n", got)
		dmp := diffmatchpatch.New()
		diffs := dmp.DiffMain(got, want, false)
		s := dmp.DiffPrettyText(diffs)
		t.Fatalf("found a difference: %s", s)
	}
}
