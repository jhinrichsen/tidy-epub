module tidy-epub

go 1.22.3

require (
	github.com/editorconfig/editorconfig-core-go/v2 v2.6.2
	github.com/sergi/go-diff v1.3.1
	golang.org/x/net v0.25.0
)

require (
	golang.org/x/mod v0.16.0 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
