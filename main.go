package main

import (
	"io"
	"log"
	"os"
	"regexp"
	"strings"

	"golang.org/x/net/html"
)

func main() {
	if err := Tidy(os.Stdin, os.Stdout); err != nil {
		log.Fatal(err)
	}
}

func Tidy(r io.Reader, w io.Writer) error {
	doc, err := html.Parse(r)
	die(err)

	var f func(*html.Node, uint) bool
	f = func(e *html.Node, level uint) bool {
		/*
			for range level {
				fmt.Printf("  ")
			}
			fmt.Printf("* * * node %p: %+v\n", e, e)
		*/

		// page number?
		if isPageNumber(e) {
			// backtrack and retry
			e = e.PrevSibling
			e.Parent.RemoveChild(e.NextSibling) // p
			e.Parent.RemoveChild(e.NextSibling) // newline
			return true
		}

		// merge two <p/>?
		for isDuplicateP(e) {
			mergeText(e)
			e.Parent.RemoveChild(e.NextSibling.NextSibling) // crlf, p
			// empty newline following?
			if !hasText(e.NextSibling.NextSibling) {
				e.Parent.RemoveChild(e.NextSibling.NextSibling) // crlf, p, \n
			}
		}

		level++
	repeat:
		for c := e.FirstChild; c != nil; c = c.NextSibling {
			backtrack := f(c, level)
			if backtrack {
				goto repeat
			}
		}
		return false
	}
	f(doc, 0)

	if err := html.Render(w, doc); err != nil {
		log.Fatal(err)
	}
	return err
}

func die(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func final(s string) bool {
	if strings.HasSuffix(s, ", ") {
		return false
	}
	return strings.HasSuffix(s, " ") ||
		strings.HasSuffix(s, "”") // closing direct speech
}

func hasText(e *html.Node) bool {
	return e.Type == html.TextNode && len(strings.TrimSpace(e.Data)) > 0
}

func isDuplicateP(e *html.Node) bool {
	// debug friendly list of predicates
	if !isParagraph(e) {
		return false
	}
	if e.FirstChild == nil {
		return false
	}
	if e.FirstChild.Type != html.TextNode {
		return false
	}
	if final(e.FirstChild.Data) {
		return false
	}

	e = e.NextSibling // newline
	e = e.NextSibling // p
	if !isParagraph(e) {
		return false
	}
	if e.FirstChild == nil {
		return false
	}
	if e.FirstChild.Type != html.TextNode {
		return false
	}
	return true
}

func isPageNumber(e *html.Node) bool {
	re := regexp.MustCompile(`\d+/\d+`)
	return isParagraph(e) &&
		e.FirstChild != nil &&
		e.FirstChild.NextSibling != nil &&
		e.FirstChild.NextSibling.Type == html.TextNode &&
		re.MatchString(e.FirstChild.NextSibling.Data)
}

func isParagraph(e *html.Node) bool {
	if e == nil {
		return false
	}
	return e.Type == html.ElementNode &&
		strings.ToLower(e.Data) == "p"
}

func mergeText(e *html.Node) {
	const hyphen = "-" // not to be confused with "—"
	this := e.FirstChild.Data
	that := e.NextSibling.NextSibling.FirstChild.Data // crlf, p, text
	if strings.HasSuffix(this, hyphen) {
		this = strings.TrimRight(this, hyphen)
		this += that
	} else {
		this += " " + that
	}
	e.FirstChild.Data = this
}
