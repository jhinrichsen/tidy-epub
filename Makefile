.PHONY: diff
diff:
	go run main.go < testdata/original.html | diff --color -w --width=200 -y testdata/nice.html -
